# Nano
###### IMPORTANT: This bot is programmed to be used on Heroku. I will not provide assistance if you want to run it yourself.
A anime bot for Discord based on the character Shinonome Nano from Nichijou. 

I try to keep this bot as appropriate as possible for younger users. If Nano replies with anything profane or suggestive, I ***urge*** you to report it on the [official server](https://discord.gg/BCqn8bF). I, and the rest of the team, will try to remove it as quickly as possible.

### Links
###### [Official Discord Server](https://discord.gg/BCqn8bF)
###### [Invite Bot](https://discordapp.com/oauth2/authorize?client_id=361374175446827019&scope=bot&permissions=8)

## License
DISCLAIMER. THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OR CONDITION, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. LICENSORS HEREBY DISCLAIM ALL LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE.
